package sheridan;


public class Rectangle {
    private double width;
    private double height;

    public Rectangle() {
    }
    
    

    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setHeight(double height) {
        this.height = height;
    }
    
    public double getArea(){
    return this.height* this.width;
    }
    
    public final static void setDimensions(Rectangle r, double w, double h){
    
    r.setWidth(w);
    r.setHeight(h);
    }
}